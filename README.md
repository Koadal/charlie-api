Installation du projet :


1 - git clone https://gitlab.com/Koadal/charlie-api.git

2 - composer install 

3 - Créez une base de données "charlie-api"

4 - php artisan migrate 

5 - php artisan db:seed

6 - php artisan serve

7 -  http://127.0.0.1:8000    Carte des entreprise

8 - http://127.0.0.1:8000/api/entreprise API avec un get sur les entreprises
