<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntreprisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entreprises', function($table) {
            $table->id();
            $table->string('nom_entreprise');
            $table->string('adresse');
            $table->string('tel_entreprise');
            $table->decimal('latitude',8,6);
            $table->decimal('longitude',9,6);
        });
        Schema::create('employés', function($table){
            $table->id();
            $table->integer('id_entreprise')->unsigned();
            $table->foreign('id_entreprise')->references('id')->on('entreprise');
            $table->string('nom');
            $table->string('prenom');
            $table->string('adresse');
            $table->string('tel_employé');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprises');
    }
}
