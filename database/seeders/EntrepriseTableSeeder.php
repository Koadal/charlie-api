<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntrepriseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // creation de 2 entreprise
        DB::table('entreprises')->insert([
            'nom_entreprise' => 'Charlie Solution',
            'adresse' => '16 rue de lille',
            'tel_entreprise' => '0610740448',
            'latitude' => '50.633333',
            'longitude' => '3.066667'            
        ]);
        DB::table('entreprises')->insert([
            'nom_entreprise' => 'Alpha Solution',
            'adresse' => '6 rue Halévy',
            'tel_entreprise' => '071544645',
            'latitude' => '50.6248808',
            'longitude' => '3.0383962'            
        ]);
        DB::table('employés')->insert([
            'id_entreprise' => '1',
            'nom' => 'Coadou',
            'prenom' => 'Julien',
            'adresse' => '76 rue leon blum dunkerque',
            'tel_employé' => '0687214545'            
        ]);
        DB::table('employés')->insert([
            'id_entreprise' => '1',
            'nom' => 'Smith',
            'prenom' => 'George',
            'adresse' => '45 rue de france',
            'tel_employé' => '0745214878'            
        ]);
        DB::table('employés')->insert([
            'id_entreprise' => '2',
            'nom' => 'Porte',
            'prenom' => 'Noel',
            'adresse' => '20 du code',
            'tel_employé' => '058746478'            
        ]);
        DB::table('employés')->insert([
            'id_entreprise' => '2',
            'nom' => 'Spiderman',
            'prenom' => 'Jean Louis',
            'adresse' => '20 rue du dieu JL',
            'tel_employé' => '0627272748'            
        ]);
    }
}